
USE book_list;

DROP TABLE IF EXISTS employee_list;
CREATE TABLE employee_list (
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	firstName VARCHAR(100) NOT NULL,
	lastName VARCHAR(100) NOT NULL,
	age INTEGER NOT NULL,
	gender VARCHAR(1) NOT NULL,
	position VARCHAR(100) NOT NULL,
    shift VARCHAR(100) NOT NULL,
	availability VARCHAR(100) NOT NULL,
	rate DECIMAL(7, 2) NOT NULL);

SELECT 'INSERTING DATA INTO DATABASE' as 'INFO';

INSERT INTO employee_list VALUES ( null, 'Joe', 'Mullins', 64, 'M', 'Cashier','Morning','part-time', 63.08);
INSERT INTO employee_list VALUES ( null, 'Joan', 'Macgill', 27, 'F', 'Cashier', 'Evening', 'part-time',38);
INSERT INTO employee_list VALUES ( null, 'Jim', 'Mitchell', 51, 'M', 'Clerk','Afternoon', 'Full-time', 38);
INSERT INTO employee_list VALUES ( null, 'John', 'Magner', 47, 'M', 'Customer Service', 'Morning','part-time', 63.08);
INSERT INTO employee_list VALUES ( null, 'Jean', 'Madden', 45, 'F', 'Stock Handler', 'Evening','Full-time', 76.45);
INSERT INTO employee_list VALUES ( null, 'Jack', 'Minogue', 61, 'M', 'Clerk','Afternoon','part-time',  45.57);
INSERT INTO employee_list VALUES ( null, 'Josephine', 'Mahony', 33, 'F', 'Store Manager','Full Day', 'Full-time', 98.56);
INSERT INTO employee_list VALUES ( null, 'Juan', 'Mosley', 56, 'M', 'Clerk', 'Evening','part-time', 76.45);
INSERT INTO employee_list VALUES ( null, 'Jamie', 'Mulllen', 45, 'M', 'Cashierr', 'Morning','part-time', 38);
INSERT INTO employee_list VALUES ( null, 'Nicole', 'Ng', 26, 'F', 'Toilet CLeaner', 'Afternoon', 'part-time',63.08);
INSERT INTO employee_list VALUES ( null, 'Dexter', 'Gwee', 24, 'M', 'Music Section Clerk', 'Evening Night', 'Full-time',63.08);
INSERT INTO employee_list VALUES ( null, 'Christie', 'Choong', 22, 'F', 'Adult Section Clerk', 'Night', 'part-time',12.00);
INSERT INTO employee_list VALUES ( null, 'Darren', 'Chow', 16, 'M', 'Clerk', 'Evening', 'part-time',10.00);
INSERT INTO employee_list VALUES ( null, 'Shadrach', 'MCDaddy', 45, 'M', 'Kid Section Clerk', 'Morning', 'Full-time',140.00);
INSERT INTO employee_list VALUES ( null, 'Adrian', 'Tan', 32, 'M', 'Cleaner', 'Morning Night', 'Full-time',01.50);



SET FOREIGN_KEY_CHECKS = 1;

import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.*;
import java.sql.*;

@SuppressWarnings("serial")
public class JDBCMainWindowContent extends JInternalFrame implements ActionListener
{	
	String cmd = null;

	private Connection con = null;
	private Statement stmt = null;
	private Statement stmt2 = null;
	private ResultSet rs = null;

	private Container content;
	private JPanel detailsPanel;
	JPanel exportButtonPanel;
	private JPanel ButtonPanel;
	JPanel ExportPanel;
	private JScrollPane dbContentsPanel;
	private JScrollPane dbContentsPanel2;
	private JTabbedPane TabPanel;
	private Border lineBorder;
	private Border lineBorder2;
	
	private JLabel IDLabel=new JLabel("ID:                 ");
	private JLabel TitleLabel=new JLabel("Title:               ");
	private JLabel AuthorLabel=new JLabel("Author:        ");
	private JLabel GenreLabel=new JLabel("Genre:                 ");
	private JLabel AudLabel=new JLabel("Audience:               ");
	private JLabel PubLabel=new JLabel("Publisher:      ");
	private JLabel PrcLabel=new JLabel("Price:      ");

	 JTextField IDTF= new JTextField(10);
	 JTextField TitleTF=new JTextField(10);
	 JTextField AuthorTF=new JTextField(10);
	 JTextField GenreTF=new JTextField(10);
	 JTextField AudTF=new JTextField(10);
	 JTextField PubTF=new JTextField(10);
	 JTextField PrcTF=new JTextField(10);
	 JTextField TF=new JTextField(10);

	private static QueryTableModel TableModel = new QueryTableModel();
	private static QueryTableModel TableModel2 = new QueryTableModel();
	//Add the models to JTabels
	private JTable TableofDBContents=new JTable(TableModel);
	private JTable TableofDBContents2=new JTable(TableModel2);
	//Buttons for inserting, and updating members
	//also a clear button to clear details panel
	private JButton updateButton = new JButton("Update");
	private JButton insertButton = new JButton("Insert");
	private JButton deleteButton  = new JButton("Delete");
	private JButton clearButton  = new JButton("Clear");

	private JButton  ShowBook = new JButton("Book List");
	private JButton ShowEmployee  = new JButton("Employee List");
	private JButton but1  = new JButton("Total Books");
	private JButton but2  = new JButton("BLank");
	private JButton ListAllPositions  = new JButton("ListAllPositions");
	private JButton ListMorning = new JButton("Morning Shift: ");
	private JButton ListAfternoon = new JButton("Afternoon Shift: ");
	private JButton ListEvening = new JButton("Evening Shift: ");

	public JDBCMainWindowContent( String aTitle)
	{	
		//setting up the GUI
		super(aTitle, false,false,false,false);
		setEnabled(true);

		initiate_db_conn();
		//add the 'main' panel to the Internal Frame
		content=getContentPane();
		content.setLayout(null);
		content.setBackground(new Color(66,66,66));
		lineBorder = BorderFactory.createEtchedBorder(25,new Color(255,61,0), new Color(255,61,0));

		//setup details panel and add the components to it
		detailsPanel=new JPanel();
		detailsPanel.setLayout(new GridLayout(11,2));
		detailsPanel.setBackground(new Color(158,158,158));
		detailsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Details"));
		((TitledBorder) detailsPanel.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));
		IDLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));

		detailsPanel.add(IDLabel);			
		detailsPanel.add(IDTF);
		TitleLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(TitleLabel);		
		detailsPanel.add(TitleTF);
		AuthorLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(AuthorLabel);		
		detailsPanel.add(AuthorTF);
		GenreLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(GenreLabel);	
		detailsPanel.add(GenreTF);
		AudLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(AudLabel);		
		detailsPanel.add(AudTF);
		PubLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(PubLabel);
		detailsPanel.add(PubTF);
		PrcLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(PrcLabel);
		detailsPanel.add(PrcTF);

		exportButtonPanel=new JPanel();
		exportButtonPanel.setLayout(new GridLayout(8,1));
		exportButtonPanel.setBackground(new Color(158,158,158));
		exportButtonPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Export Data"));
		((TitledBorder) exportButtonPanel.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));
		exportButtonPanel.add(ShowBook);
		exportButtonPanel.add(ShowEmployee);
		exportButtonPanel.add(but1);
		exportButtonPanel.add(but2);
		exportButtonPanel.add(ListAllPositions);
		exportButtonPanel.add(ListMorning);
		exportButtonPanel.add(ListEvening);
		exportButtonPanel.add(ListAfternoon);
		exportButtonPanel.setSize(301, 442);
		exportButtonPanel.setLocation(5, 5);
		ExportPanel = new JPanel();
		ExportPanel.setLayout(null);
		ExportPanel.setBackground(new Color(66,66,66));
		ExportPanel.add(exportButtonPanel);
		TF.setBounds(320, 400, 700, 47);
		ExportPanel.add(TF);
		insertButton.setFont(new Font("Tahoma", Font.BOLD, 14));

		insertButton.setSize(100, 30);
		updateButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		updateButton.setSize(100, 30);
		deleteButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		deleteButton.setSize (100, 30);
		clearButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		clearButton.setSize (100, 30);

		insertButton.addActionListener(this);
		updateButton.addActionListener(this);
		deleteButton.addActionListener(this);
		clearButton.addActionListener(this);
		ShowBook.addActionListener(this);
		ShowEmployee.addActionListener(this);
		but1.addActionListener(this);
		ShowBook.addActionListener(this);
		ListMorning.addActionListener(this);
		ListEvening.addActionListener(this);
		ListAfternoon.addActionListener(this);

		lineBorder2 = BorderFactory.createEtchedBorder(15,new Color(255,61,0), new Color(255,61,0));
		ButtonPanel = new JPanel();
		ButtonPanel.setBorder(BorderFactory.createTitledBorder(lineBorder2, "Actions"));
		((TitledBorder) ButtonPanel.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));
		ButtonPanel.setBackground( new Color(158,158,158));
		ButtonPanel.setSize(600, 87);
		ButtonPanel.setLocation(200, 320);
		ButtonPanel.add(insertButton);
		ButtonPanel.add(updateButton);
		ButtonPanel.add(deleteButton);
		ButtonPanel.add(clearButton);

		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));
		TableofDBContents2.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel=new JScrollPane(TableofDBContents,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel.setBackground(new Color(158,158,158));
		dbContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder,"Database Content"));
		((TitledBorder) dbContentsPanel.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));
		
		dbContentsPanel2=new JScrollPane(TableofDBContents2,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel2.setBackground(new Color(158,158,158));
		dbContentsPanel2.setBorder(BorderFactory.createTitledBorder(lineBorder,"Database Content"));
		((TitledBorder) dbContentsPanel2.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));

		detailsPanel.setSize(378, 309);
		detailsPanel.setLocation(3,0);
		dbContentsPanel.setSize(700, 309);
		dbContentsPanel.setLocation(391, 0);
		dbContentsPanel2.setSize(700, 394);
		dbContentsPanel2.setLocation(320, 5);

		content.add(detailsPanel);
		content.add(dbContentsPanel);
		content.add(ButtonPanel);
		ExportPanel.add(dbContentsPanel2);

		setSize(982,645);
		setVisible(true);

		TableModel.refreshFromDB(stmt);
	}

	public void initiate_db_conn()
	{
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/book_list";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "02071997");
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
			
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}

	//event handling 
	public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		if (target == clearButton)
		{
			IDTF.setText("");
			TitleTF.setText("");
			AuthorTF.setText("");
			GenreTF.setText("");
			AudTF.setText("");
			PubTF.setText("");
			PrcTF.setText("");
		}

		if (target == insertButton)
		{		 
			try
			{
				String updateTemp ="INSERT INTO details VALUES("+
				null +",'" + TitleTF.getText()+"','"+AuthorTF.getText()+"','"+GenreTF.getText()+"','"+AudTF.getText()+"','"
				+PubTF.getText()+"',"+PrcTF.getText()+");" ;
				stmt.executeUpdate(updateTemp);
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with  insert:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
		if (target == deleteButton)
		{
			try
			{
				String updateTemp ="DELETE FROM details WHERE id = "+IDTF.getText()+";"; 
				stmt.executeUpdate(updateTemp);
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with delete:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB(stmt);
			}
		}
		if (target == updateButton)
		{	 	
			try
			{ 			
				String updateTemp ="UPDATE details SET " +
				"Title = '"+TitleTF.getText()+
				"', Author = '"+AuthorTF.getText()+
				"', Genre = '"+GenreTF.getText()+
				"', Audience ='"+AudTF.getText()+
				"', Publisher = '"+PubTF.getText()+
				"', Price = "+PrcTF.getText()+
				" where id = "+IDTF.getText();

				stmt.executeUpdate(updateTemp);
				//these lines do nothing but the table updates when we access the db.
				rs = stmt.executeQuery("SELECT * from details ");
				rs.next();
				rs.close();	
			}
			catch (SQLException sqle){
				System.err.println("Error with  update:\n"+sqle.toString());
			}
			finally{
				TableModel.refreshFromDB(stmt);
			}
		}
		if(target == this.ShowBook){		 
			TableModel2.refreshFromDB(stmt);
		}
		if(target == this.ShowEmployee){		 
			TableModel2.refreshFromDB2(stmt);
		}
		if(target == this.but1){
			cmd = "SELECT count(*) as NumberofBooks from details;";
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
				TF.setText("Success");
			}
			catch(Exception e1){e1.printStackTrace();}
		}
		if(target == this.but2){
			cmd = "SELECT count(*) as NumberofBooks from details;";
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
				TF.setText("Success");
			}
			catch(Exception e1){e1.printStackTrace();}
		}
		if(target == this.ListMorning){
			TF.setText("");
			cmd = "select firstName, lastName from employee_list where shift = 'Morning';";
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile2(rs);
				TF.setText("Success");
			}
			catch(Exception e1){e1.printStackTrace();}
		}
		if(target == this.ListAfternoon){
			TF.setText("");

			cmd = "select firstName, lastName from employee_list where shift = 'Afternoon';";
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
				TF.setText("Success");
			}
			catch(Exception e1){e1.printStackTrace();}
		}
		if(target == this.ListEvening){
			TF.setText("");

			cmd = "select firstName, lastName from employee_list where shift = 'Evening';";
			try{					
				rs= stmt.executeQuery(cmd); 	
				writeToFile(rs);
				TF.setText("Success");
			}
			catch(Exception e1){e1.printStackTrace();}
		}
	}

	void writeToFile(ResultSet rs){
		try{
			System.out.println("In writeToFile");
			FileWriter outputFile = new FileWriter("Books.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
		catch(Exception e){e.printStackTrace();}
	}
	private void writeToFile2(ResultSet rs){
		try{
			System.out.println("In writeToFile");
			FileWriter outputFile = new FileWriter("Employee List.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
		catch(Exception e){e.printStackTrace();}
	}
}

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class JDBCEmployeeWindow  extends JInternalFrame implements ActionListener
{

	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private JPanel detailsPanel;
	JPanel exportButtonPanel;
	private JPanel ButtonPanel;
	JPanel ExportPanel;
	private JScrollPane dbContentsPanel;
	private JTabbedPane TabPanel;
	private Border lineBorder;
	private Border lineBorder2;
	
	private JLabel IDLabel=new JLabel("ID:                 ");
	private JLabel FirstNLabel=new JLabel("First Name:               ");
	private JLabel LastNLabel=new JLabel("Last Name:        ");
	private JLabel AgeLabel=new JLabel("Age:                 ");
	private JLabel GenderLabel=new JLabel("Gender:               ");
	private JLabel PositionLabel=new JLabel("Position:      ");
	private JLabel ShiftLabel=new JLabel("Shift:      ");
	private JLabel AvailabilityLabel=new JLabel("Availability:      ");
	private JLabel RateLabel=new JLabel("Rate:      ");

	 JTextField IDTF= new JTextField(10);
	 JTextField FirstNTF=new JTextField(10);
	 JTextField LastNTF=new JTextField(10);
	 JTextField AgeTF=new JTextField(10);
	 JTextField GenderTF=new JTextField(10);
	 JTextField PositionTF=new JTextField(10);
	 JTextField ShiftTF=new JTextField(10);
	 JTextField AvailabilityTF=new JTextField(10);
	 JTextField RateTF=new JTextField(10);

	private static QueryTableModel TableModel = new QueryTableModel();
	private JTable TableofDBContents=new JTable(TableModel);
	private JButton updateButton = new JButton("Update");
	private JButton insertButton = new JButton("Insert");
	private JButton deleteButton  = new JButton("Delete");
	private JButton clearButton  = new JButton("Clear");
	
	public JDBCEmployeeWindow( String aTitle)
	{	
		super(aTitle, false,false,false,false);
		setEnabled(true);

		initiate_db_conn();
		content=getContentPane();
		content.setLayout(null);
		content.setBackground(new Color(66,66,66));
		lineBorder = BorderFactory.createEtchedBorder(20,new Color(255,61,0), new Color(255,61,0));

		detailsPanel=new JPanel();
		detailsPanel.setLayout(new GridLayout(11,2));
		detailsPanel.setBackground(new Color(158,158,158));
		detailsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Details"));
		((TitledBorder) detailsPanel.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));
		IDLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));

		detailsPanel.add(IDLabel);			
		detailsPanel.add(IDTF);
		FirstNLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(FirstNLabel);		
		detailsPanel.add(FirstNTF);
		LastNLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(LastNLabel);		
		detailsPanel.add(LastNTF);
		AgeLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(AgeLabel);	
		detailsPanel.add(AgeTF);
		GenderLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(GenderLabel);		
		detailsPanel.add(GenderTF);
		PositionLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(PositionLabel);
		detailsPanel.add(PositionTF);
		ShiftLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(ShiftLabel);
		detailsPanel.add(ShiftTF);
		AvailabilityLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(AvailabilityLabel);
		detailsPanel.add(AvailabilityTF);
		RateLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 16));
		detailsPanel.add(RateLabel);
		detailsPanel.add(RateTF);

		exportButtonPanel=new JPanel();
		exportButtonPanel.setLayout(new GridLayout(3,2));
		exportButtonPanel.setBackground(new Color(158,158,158));
		exportButtonPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Export Data"));
		exportButtonPanel.setSize(500, 200);
		exportButtonPanel.setLocation(3, 300);
		ExportPanel = new JPanel();
		ExportPanel.add(exportButtonPanel);
		insertButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		insertButton.setSize(100, 30);
		updateButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		updateButton.setSize(100, 30);
		deleteButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		deleteButton.setSize (100, 30);
		clearButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		clearButton.setSize (100, 30);

		insertButton.addActionListener(this);
		updateButton.addActionListener(this);
		deleteButton.addActionListener(this);
		clearButton.addActionListener(this);
		
		lineBorder2 = BorderFactory.createEtchedBorder(15, new Color(255,61,0), new Color(255,61,0));
		ButtonPanel = new JPanel();
		ButtonPanel.setBorder(BorderFactory.createTitledBorder(lineBorder2, "Actions"));
		((TitledBorder) ButtonPanel.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));
		ButtonPanel.setBackground(new Color(158,158,158));
		ButtonPanel.setSize(600, 87);
		ButtonPanel.setLocation(200, 320);
		ButtonPanel.add(insertButton);
		ButtonPanel.add(updateButton);
		ButtonPanel.add(deleteButton);
		ButtonPanel.add(clearButton);

		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));

		dbContentsPanel=new JScrollPane(TableofDBContents,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		dbContentsPanel.setBackground(new Color(158,158,158));
		dbContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder,"Database Content"));
		((TitledBorder) dbContentsPanel.getBorder()).setTitleFont(new Font("Arial", Font.ITALIC, 22));

		detailsPanel.setSize(378, 309);
		detailsPanel.setLocation(3,0);
		dbContentsPanel.setSize(700, 309);
		dbContentsPanel.setLocation(391, 0);
		//TabPanel = new JTabbedPane();

		content.add(detailsPanel);
		content.add(dbContentsPanel);
		content.add(ButtonPanel);
		//content.add(TabPanel);

		setSize(982,645);
		setVisible(true);

		TableModel.refreshFromDB2(stmt);
	}

	public void initiate_db_conn()	
	{
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Specify the DB Name
			String url="jdbc:mysql://localhost:3306/book_list";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "02071997");
			//Create a generic statement which is passed to the TestInternalFrame1
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}

	//event handling 
	public void actionPerformed(ActionEvent e)
	{
		Object target=e.getSource();
		if (target == clearButton)
		{
			IDTF.setText("");
			FirstNTF.setText("");
			LastNTF.setText("");
			AgeTF.setText("");
			GenderTF.setText("");
			PositionTF.setText("");
			ShiftTF.setText("");
			AvailabilityTF.setText("");
			RateTF.setText("");
		}

		if (target == insertButton)
		{		 
			try
			{
				String updateTemp ="INSERT INTO employee_list VALUES("+
				null +",'" + FirstNTF.getText()+"','"+LastNTF.getText()+"','"+AgeTF.getText()+"','"+GenderTF.getText()+"','"
				+PositionTF.getText()+"','"+ShiftTF.getText()+"','"+AvailabilityTF.getText()+"',"+RateTF.getText()+");" ;

				stmt.executeUpdate(updateTemp);

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with  insert:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB2(stmt);
			}
		}
		if (target == deleteButton)
		{

			try
			{
				String updateTemp ="DELETE FROM employee_list WHERE id = "+IDTF.getText()+";"; 
				stmt.executeUpdate(updateTemp);

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with delete:\n"+sqle.toString());
			}
			finally
			{
				TableModel.refreshFromDB2(stmt);
			}
		}
		if (target == updateButton)
		{	 	
			try
			{ 			
				String updateTemp ="UPDATE details SET " +
				"firstName = '"+FirstNTF.getText()+
				"', lastName = '"+LastNTF.getText()+
				"', Age = '"+AgeTF.getText()+
				"', Gender ='"+GenderTF.getText()+
				"', Position = '"+PositionTF.getText()+
				"', Shift = '"+ShiftTF.getText()+
				"', Availability = '"+AvailabilityTF.getText()+
				"', Rate = "+RateTF.getText()+
				" where id = "+IDTF.getText();


				stmt.executeUpdate(updateTemp);
				//these lines do nothing but the table updates when we access the db.
				rs = stmt.executeQuery("SELECT * from details ");
				rs.next();
				rs.close();	
			}
			catch (SQLException sqle){
				System.err.println("Error with  update:\n"+sqle.toString());
			}
			finally{
				TableModel.refreshFromDB2(stmt);
			}
			}
		}

	private void writeToFile(ResultSet rs){
		try{
			System.out.println("In writeToFile");
			FileWriter outputFile = new FileWriter("Employee.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
		catch(Exception e){e.printStackTrace();}
	}
}
